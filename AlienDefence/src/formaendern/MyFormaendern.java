package formaendern;


import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;

public class MyFormaendern extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtTest;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyFormaendern frame = new MyFormaendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyFormaendern() {
		setTitle("Form ändern");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);


		JLabel labelTextaendern = new JLabel("Dieser Text soll verändert werden.");
		labelTextaendern.setHorizontalAlignment(SwingConstants.CENTER);
		labelTextaendern.setBounds(45, 6, 380, 69);
		contentPane.add(labelTextaendern);
		
			
		JLabel labelHFarbe = new JLabel("Hintergrund ändern");
		labelHFarbe.setBounds(45, 96, 132, 16);
		contentPane.add(labelHFarbe);
		
		
		JButton btnRotH = new JButton("Rot");
		btnRotH.setBounds(45, 124, 121, 29);
		btnRotH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					getContentPane().setBackground(Color.RED);
			}
		});
		contentPane.add(btnRotH);
		
		JButton btnGrünH = new JButton("Grün");
		btnGrünH.setBounds(178, 124, 121, 29);
		btnGrünH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					getContentPane().setBackground(Color.GREEN);
			}
		});
		contentPane.add(btnGrünH);
		
		JButton btnBlauH = new JButton("Blau");
		btnBlauH.setBounds(304, 124, 121, 29);
		btnBlauH.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
				getContentPane().setBackground(Color.BLUE);
			}
		});
		contentPane.add(btnBlauH);
		
		JButton btnGelbH = new JButton("Gelb");
		btnGelbH.setBounds(45, 165, 121, 29);
		btnGelbH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getContentPane().setBackground(Color.YELLOW);
			}
		});
		contentPane.add(btnGelbH);
		
		JButton btnStandardH = new JButton("Standardfarbe");
		btnStandardH.setBounds(178, 165, 121, 29);
		btnStandardH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					getContentPane().setBackground(new Color(0xEEEEEE));
			}
		});
		contentPane.add(btnStandardH);
		
		JButton btnFarbewaehlenH = new JButton("Farbe wählen");
		btnFarbewaehlenH.setBounds(304, 165, 121, 29);
		btnFarbewaehlenH.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					getContentPane().setBackground(JColorChooser.showDialog(null, "Farbe wählen", Color.white));
			}
		});
		contentPane.add(btnFarbewaehlenH);
		

		
		JLabel labelFomartierung = new JLabel("Text Formatieren");
		labelFomartierung.setBounds(45, 206, 132, 16);
		contentPane.add(labelFomartierung);
		
		JButton btnArial = new JButton("Arial");
		btnArial.setBounds(45, 234, 121, 29);
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				labelTextaendern.setFont(new Font("Arial", Font.PLAIN, labelTextaendern.getFont().getSize()));
			}
		});
		contentPane.add(btnArial);
		
		JButton btnComicSan = new JButton("Comic San");
		btnComicSan.setBounds(178, 234, 121, 29);
		btnComicSan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				labelTextaendern.setFont(new Font("Comic Sans MS", Font.PLAIN, labelTextaendern.getFont().getSize()));
			}
		});
		contentPane.add(btnComicSan);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.setBounds(304, 234, 121, 29);
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				labelTextaendern.setFont(new Font("Courier New", Font.PLAIN, labelTextaendern.getFont().getSize()));
			}
		});
		contentPane.add(btnCourierNew);
		
		txtTest = new JTextField();
		txtTest.setText("Hier bitte den Text eingeben");
		txtTest.setToolTipText("");
		txtTest.setBounds(45, 275, 380, 26);
		contentPane.add(txtTest);
		txtTest.setColumns(10);
		
		JButton btnInsLabelSchrieben = new JButton("Ins Label schrieben");
		btnInsLabelSchrieben.setBounds(45, 313, 180, 29);
		btnInsLabelSchrieben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				labelTextaendern.setText(txtTest.getText());
			}
		});
		contentPane.add(btnInsLabelSchrieben);
		
		JButton btnTextImLabel = new JButton("Text im Label löschen");
		btnTextImLabel.setBounds(245, 313, 180, 29);
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				labelTextaendern.setText(null);
			}
		});
		contentPane.add(btnTextImLabel);
		
		JLabel lblSchriftfarbendern = new JLabel("Schriftfarbe ändern");
		lblSchriftfarbendern.setBounds(45, 360, 132, 16);
		contentPane.add(lblSchriftfarbendern);
		
		JButton btnRot = new JButton("Rot");
		btnRot.setBounds(45, 388, 121, 29);
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				labelTextaendern.setForeground(Color.RED);
			}
		});
		contentPane.add(btnRot);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.setBounds(178, 388, 121, 29);
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				labelTextaendern.setForeground(Color.BLUE);
			}
		});
		contentPane.add(btnBlau);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.setBounds(304, 388, 121, 29);
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				labelTextaendern.setForeground(Color.BLACK);
			}
		});
		contentPane.add(btnSchwarz);
		
		JLabel lblSchriftfgroeße = new JLabel("Schriftgröße verändern");
		lblSchriftfgroeße.setBounds(45, 429, 156, 16);
		contentPane.add(lblSchriftfgroeße);
		
		JButton btnMinus = new JButton("-");
		btnMinus.setBounds(245, 457, 180, 29);
		btnMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				labelTextaendern.setFont(new Font( labelTextaendern.getFont().getFamily(), Font.PLAIN, labelTextaendern.getFont().getSize() - 1));
			}
		});
		contentPane.add(btnMinus);
		
		JButton btnPlus = new JButton("+");
		btnPlus.setBounds(45, 457, 180, 29);
		btnPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				labelTextaendern.setFont(new Font( labelTextaendern.getFont().getFamily(), Font.PLAIN, labelTextaendern.getFont().getSize() + 1));
			}
		});
		contentPane.add(btnPlus);
		
		JLabel lblTextausrichtung = new JLabel("Textausrichtung");
		lblTextausrichtung.setBounds(45, 498, 132, 16);
		contentPane.add(lblTextausrichtung);
		
		JButton btnLinksbndig = new JButton("linksbündig");
		btnLinksbndig.setBounds(45, 526, 121, 29);
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				labelTextaendern.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		contentPane.add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.setBounds(178, 526, 121, 29);
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				labelTextaendern.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		contentPane.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsbündig");
		btnRechtsbndig.setBounds(304, 526, 121, 29);
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				labelTextaendern.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		contentPane.add(btnRechtsbndig);
		
		JLabel lblProgrammbeenden = new JLabel("Programm beenden");
		lblProgrammbeenden.setBounds(45, 567, 132, 16);
		contentPane.add(lblProgrammbeenden);
		
		JButton btnExit = new JButton("Exit");
		btnExit.setBounds(45, 595, 380, 69);
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(EXIT_ON_CLOSE);
			}
		});
		contentPane.add(btnExit);
	}
}
